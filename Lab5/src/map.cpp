//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 5
// Author:     Stuart Enters
//

#include "map.h"
#include "cell.h"
#include <iostream>
using namespace std;


  // initialize empty map
  Map::Map(){
      for (int i = 0; i < WIDTH; ++i){
          for (int j = 0; j < HEIGHT; ++j){
              cells[i][j] = new MapCell(i,j,' ');
          }
      }
  }


  // read the map from cin
  void Map::load(){
      string line;

      // For each line of the map,
      // get a line of input from the user
      for (int i = 0; i < 12; ++i){
          getline(cin, line);

          // Parse the input other than
          // the first or last
          if (i > 0 && i < 11){
              parseLine(line.substr(1, line.length() - 2), i - 1);
          }
      }
  }


  // Parse a single line of the input
  void Map::parseLine(string line, int lineNum){
      // For every significant char, make the corresponding cell that
      // value
      for (int i = 0; i < 20; ++i){
          (cells[lineNum][i]) = new MapCell(lineNum, i, line[i]);
      }
  }


  MapCell* Map::getCell(int x, int y){
      return cells[y][x];
  }


  // write the full map to cout
  void Map::write(){
      cout << "+--------------------+" << endl;

      // For each line of the map, wrap in pipes
      for (int i = 0; i < 10; ++i){
          cout << '|';

          // Print out every character in the map
          for (int j = 0; j < 20; ++j){
              cout << cells[i][j]->display();
          }

          cout << '|' << endl;
      }

      cout << "+--------------------+" << endl;
  }


