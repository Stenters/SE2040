//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 5
// Author:     Stuart Enters
//

#include "cell.h"



  MapCell::MapCell(int x, int y, char type){
	  xLocation = x;
	  yLocation = y;
	  token = type;
	  tempToken = type;
	  hasRobot = false;
  }


  // true if mountain at this location
  bool MapCell::hasMountain(){
	  return token == '#';
  }


  // true if gold at this location
  bool MapCell::hasGold(){
	  return token == '*';
  }

  // remove the gold at this location
  void MapCell::removeGold(){
	  token = ' ';
  }


  // is this location occupied by something that the robot
  //   cannot move over?
  bool MapCell::occupied(){
	  return token == '#';
  }


  // return the character to display for this location
  char MapCell::display(){
	  return token;
  }


  // robot enters location
  void MapCell::enter(){
	  if (hasGold()){
		  removeGold();
	  }

	  tempToken = token;
	  token = 'R';
	  hasRobot = true;
  }


  // robot leaves location
  void MapCell::vacate(){
	  token = tempToken;
	  hasRobot = false;
  }


  // current X location
  int MapCell::x(){
	  return xLocation;
  }


  // current Y location
  int MapCell::y(){
	  return yLocation;
  }
