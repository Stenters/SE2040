/*
 * pointerTest.cpp
 *
 *  Created on: Mar 22, 2019
 *      Author: enterss
 */

#include <iostream>
#include <ctime>
using namespace std;


int* doSomething(){
    int *pointer;
    static int arry[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    pointer = arry;
    cout << "pointer is " << pointer << endl << "{ " << pointer[0];

    for (int i = 1; i < 10; ++i){
        cout << ", " << pointer[i];
    }
    cout << "}" << endl;
    return pointer;
}

int main(){
    int *pointer = doSomething();
    cout << "pointer is " << pointer << endl << "{ " << pointer[0];

    for (int i = 1; i < 10; ++i){
        cout << ", " << pointer[i];
    }
    cout << "}" << endl;

    return 0;
}
