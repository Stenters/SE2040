//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab3
// Author:     Stuart Enters
//

// TODO: Comments, spacing, re-read requirements

#include <map>
#include <vector>
#include <iostream>
using namespace std;


/**
 * Function for generating the horizontal axis of the histogram
 * Axis grows with maxReps / 10
 *
 * @param maxReps the maximum repetitions of a value in the histogram
 * @param res the string to write the axis to
 */
void generateXAxis(int maxReps, string& res) {
    string count = " 0 ";
    res += "  +";

    for (int j = 0; j < maxReps; j += 5) {
        res += "----+";
        count += "   ";
        count += std::to_string(j + 5);
        if (j == 0) {
            count += " ";
        }
    }
    res += "\n   ";
    res += count;
}


/**
 * Function for generating the vertical axis of the histogram
 * Axis grows with the range of values
 *
 * @param size the range of legal values for this histogram
 * @param offset the offset between 0 and the min value of the histogram
 * @param vals an array containing the number of times each value occurred in the input
 * @param res the string to write the axis to
 */
void generateYAxis(int size, int offset, int vals[], string& res) {
    for (int i = size; i > -1; --i) {
        res += std::to_string(i + offset);
        res += " |";
        for (int j = 0; j < vals[i]; j++) {
            res += "#";
        }
        if (size + offset > 9 && i + offset < 11){
            res += "\n  ";
        } else {
        res += "\n ";
        }
    }
}


/**
 * Function for generating the histogram
 *
 * @param vals an array containing the number of time each value in the range was entered by the user
 * @param offset the offset between 0 and the minimum legal value
 * @param size the size of the array
 */
string generateHistogram(int* vals, int offset, int size){
    int maxReps = 10;

    for (int i = 0; i < size; ++i) {
        maxReps = vals[i] > maxReps ? vals[i] : maxReps;
    }

    maxReps % 10 == 0 ? maxReps : maxReps += (10 - maxReps % 10);
    string res = " ";

    generateYAxis(size, offset, vals, res);
    generateXAxis(maxReps, res);
    return res;
}


/**
 * Function for reading input from the user in a given range
 *      and recording the occurrences of values in that range
 */
int* getInput(int min, int max, int* values) {
    int val = 0;
    // Read until EoF
    while (cin >> val) {

        // If the value is not in range, print an err
        if (val < min || val > max) {
            cout << "Error: value " << val << " is out of range" << endl;

        // Else increment count
        } else {
            values[val - min]++;
        }
    }
    return values;
}

/**
 * Function for reading in inputs and printing a histogram of
 *      how often each value is entered in a given range
 */
int main(){
    // Init min and max, then create the array
    int min = 0, max = 0;
    cin >> min >> max;
    int values[max - min + 1] = {};

    // Get input from the user, then generate a histogram based on that input
    getInput(min, max, values);
    cout << generateHistogram(values, min, max - min);
    return 0;
}
