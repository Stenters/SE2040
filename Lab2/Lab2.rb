# Course:     SE 2040
# Term:       Spring 2019
# Assignment: Lab 2
# Author:     Stuart Enters

# Big -  0 Analysis


class Line
  attr_reader :words
  
  
  def initialize(line, lineNum)
    @words = []
    line.split(" ").each {|x| words << [x,lineNum]}
  end
  
  
  def getWords
    @words.map { |x,y| x }
  end
  
  
end


class Document
  attr_reader :lines
  
  
  def initialize
    @lines = []
    i = 0
    
    while line = gets
        @lines << Line.new(line, i += 1)
    end
  end
  
  
  # Obtain the formatted output from the Analyzer
  def analyze
    outString = formatOut
    analyzer = Analysis.new(self)
    analyzed = analyzer.analyze
    outString += analyzer.formatOut(analyzed)
    return outString
  end
  
  
  # Format the output to place 8 characters on a line
  def formatOut
    outString = ""
    getAllWords.each_slice(8).to_a.each { |x| outString +=  x.join(" ") + "\n" }
    return outString
  end
  
  
  # Get all the words in the document
  def getAllWords
    newArry = []
    @lines.each { |x| newArry << x.getWords() }
    return newArry.flatten! != nil ? newArry : []
  end
  
  
end


class Analysis
  attr_reader :ommittedWords
  attr_reader :doc
  
  
  def initialize(doc)
    @ommittedWords = ["a", "the", "is", "are", "i", "you", "he", "she", "it", "they", "and"]
    @doc = doc
  end
  
  
  def analyze
    res =  [] << @doc.getAllWords << (@doc.getAllWords.length / 8.0).ceil << getBigrams(@doc.getAllWords)
  end
  
  
  # Get all bigrams, that is all instances of (a,b), where (a,b) occure more than once
  def getBigrams(words)  
    words = standardizeWords(words)
    allBigrams = []
    
    words.each_index {|x| generateBigramResults(words, allBigrams, x)}   
    allBigrams.map! { |x,y| x}
    return allBigrams.uniq! != 0 ? allBigrams.join("\n") : ""
  end
  
  
  # Iterate through the document, finding bigrams and formatting them
  def generateBigramResults(words, allBigrams, i)
  comparator = [words[i-1],words[i]]
      res = checkPair(comparator, i)
     
      if res.length > 1
        allBigrams << ["#{comparator.join(" ")}: #{res.join(", ")}", res.length]
        allBigrams.sort! { |x,y| (x[1] <=> y[1]) != 0 ? -(x[1] <=> y[1]) : (x[0] <=> y[0]) }
      end
  end
  
  
  # Check the standardized array for a specific combination of letters
  def checkPair(comparator, index)
    words = []
    @doc.lines.each { |x| x.words.each { |x,y| words << [standardizeWords([x]),y] }}
    array = words.select { |x| x[0] != [] }
    
    res = array.select.with_index do |i, j|
      if j < (array.length - 1)
        [array[j][0],array[j+1][0]].flatten == comparator
        
      end
    end
    return res.map! { |x| x[1] }
  end

  
  def standardizeWords(words)
    words.map! { |x| elimChars(x.downcase) }
    words -= @ommittedWords
  end  


  def elimChars(word)
    while (word =~ /[.,:;]/) != nil
      word.sub!(/[.,:;]/,'')
      
    end
    return word
  end
  
  
  def formatOut(ary)
    return "---------- analysis ----------
Output line count: #{ary[1]}
Repeated phrases:
#{ary[2]}"
  end
  
  
end


def main
  doc = Document.new
  puts doc.analyze
end

main