/*
 * main.c
 *
 *  Created on: May 12, 2019
 *      Author: enterss
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define MAXLEN 100
#define PLUS " + "
#define MINUS " - "

int* parseStr(char* poly);
void compute(int* p1, int* p2, int* ans);
void printPoly(int* p);
void fill(int* arr, int val);
int nextTerm(char* arr);
int* parseTerm(int* res, char* poly);
void getPoly(int* poly, char* msg);

int main( int argc, const char* argv[] ) {
    bool run = true;

    while (run) {
        int poly1[MAXLEN], poly2[MAXLEN], ans[MAXLEN];
        getPoly(poly1, "Enter the first polynomial or q: ");
        run = poly1[0] == '\0';
        getPoly(poly2, "Enter the second polynomial: ");
        compute(poly1, poly2, ans);

    }
}

int* parseStr(char* poly) {
    int res[MAXLEN];
    fill (res, 0);

    if (poly[0] == 'q' || poly[0] == 'Q') {
        return '\0';

    } else {
        return parseTerm(res, poly);
    }

}

void compute(int* p1, int* p2, int* ans) {

    for (int i = 0; i < MAXLEN; ++i){
            ans[i] = p1[i] + p2[i];

    }

    printf("--------------------------------------------------");
    printPoly(ans);

}

void printPoly(int* p) {
    p[0] != 0 ? printf("%d ", p[0]) : printf("%s", "");

    for (int i = 0; p[i] < MAXLEN; ++i) {
        (p[i] != 0 && p[i] > 0) ? printf("+ %d ", p[i]) : p[i] < 0 ? printf("- %d ", p[i]) : printf("%s", "");
    }
}

void fill(int* arr, int val) {
    for (int i = 0; i < MAXLEN; ++i) {
        arr[i] = val;
    }
}

int nextTerm(char* arr) {
    int start = 0;
    while(arr[start] != '+' && arr[start] != '-' && arr[start] != '\n'){
            ++start;

            if (start == MAXLEN){
                return -1;

            }
        }
    return start;
}

int* parseTerm(int* res, char* poly) {
    res[0] = atoi(poly);
    int offset = nextTerm(poly), coeff = 0, deg = 0;

    while(offset != -1) {
        int sign = poly[offset] == '+' ? 1 : -1;
        sscanf(&(poly[offset + 2]), "%dx%d", &coeff, &deg);
        res[deg] = sign * coeff;
        offset = nextTerm(poly + offset);
    }
    return res;
}

void getPoly(int* poly, char* msg){
        char in[MAXLEN];
        printf(msg);
        fgets(in, MAXLEN, stdin);

        memcpy( parseStr(in), poly, MAXLEN);
}
