//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 4
// Author:     Stuart Enters
//

#include <iostream>
using namespace std;


/**
 * Class for containing a pool of names to compare for matches
 */
class Pool{

private:
    static int constexpr MAX_ARR_SIZE = 20;
    string arr[MAX_ARR_SIZE] = {};
    int size;


    /**
     * Method to check if two strings have at least one char in common
     * @param str1 the first string to compare
     * @param str2 the second string to compare
     */
    bool checkEquals(string* str1, string* str2){
        // Check strings for exact match
    	if (*str1 == *str2){
    		return false;
    	}

        // For each char in str1, check for matching char in str2
        for (unsigned i = 0; i < str1->length(); ++i){
            if (str2->find(str1->at(i)) != string::npos){
                return true;
            }
        }
        // No matching chars = no match
        return false;
    }

    /**
     * Method for removing extra chars from a name
     */
    string standardizeName(string name) {
        unsigned pos = name.find("_");

        // While there is a '_' char, remove it
        while (pos != string::npos) {
            name.erase(name.begin() + pos);
            pos = name.find("_");
        }

        return name;
    }

public:


    /**
     * Constructor for Pool
     */
   Pool(){
       size = 0;
    }


   /**
    * A method for reading in a stream of names into a pool
    */
    void readNames() {
        string read = "";
        cin >> read;

        // Read while the array is not full, there's no
        // End of File character, and the user didn't
        // type "END"
        while (size < 20 && !cin.eof() && read != "END"){
            add(read);
            cin >> read;
        }

    }


    /**
     * Method for adding a name to the pool
     */
    void add(string name){
        arr[size] = name;
        ++size;
    }


    /**
     * Method for printing all the matches between a given name and the pool
     */
    void printMatches(string name){
        string newName = standardizeName(name);

        for (int i = 0; i < size; ++i){

            if (checkEquals(&newName, &(arr[i]))){
                cout << "A perfect match for " << name << ": " << arr[i] << endl;
            }
        }

    }


    /**
     * Method for printing out the matches between two pools
     */
    void printMatches(Pool* pool){
        // For every name in the pool, print its matches
        for (int i = 0; i < size; ++i){
            pool->printMatches(arr[i]);
        }
    }


    /**
     * Getter if the pool is empty
     */
    bool empty(){
        return size == 0;
    }


};
