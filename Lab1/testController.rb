def main(start, final)
  ins = []
  outs = []
  results = []
  lab = Lab1.new
  
  count = start
  while count <= final
    file1 = "C:/Projects/SE2040/Lab1/in.#{count}"
    ins << file1
    file2 = "C:/Projects/SE2040/Lab1/out.#{count}"
    outs << file2
    
    file3 = createOutput(file1, lab)
    
    results << compareFiles(file2, file3)
    
    count += 1
  end
  
  puts results
    
end


def createOutput(file, lab)
  input = []
  
  File.open(file, "r") do |f|
    f.each_line do |line|
      input << line
    end
  end
  
  lab.findStats(input)
end


def compareFiles(file1, file2)
  text1 = ""
  text2 = ""
  File.open(file1, "r") do |f|
    f.each_line do |line|
      text1 += line
    end
  end

  File.open( file2, "r") do |f|
    f.each_line do |line|
      text2 += line
    end
  end
  
  puts text1
  
  return text1 == text2
  
end

require "C:/Projects/SE2040/Lab1/Lab1.rb"
main(1,10)