#
# hw1.rb: Ruby exercises
#

#----------------------------------------------------------------------
# 1. define transactions_per_millisecond here
def transactions_per_millisecond(mili, hours)
  # (25,2) = 3.47
  return ((mili * 1e6) / (hours * 3.6e6)).round(2)
  
end

#----------------------------------------------------------------------
# 2. define largest_difference here
def largest_difference(array)
  if array.length == 1
    return nil
  else
  return array.map.with_index { |x, i| [x - array[i-1], array[i-1], x] }.max[1,2]
  end
end

#----------------------------------------------------------------------
# 3. define here_to_there here
def here_to_there (array, start, fin)
  if array.index(start) == nil
    return []
  else if array.index(fin) != nil
    newAry = array.drop(array.index(start))
    return newAry[0 .. newAry.index(fin)]
  else
    return array
    end
  end
end

#----------------------------------------------------------------------
# make no changes below
# main function: calls other functions as indicated

def apply_operation_to_array(cmd, items)
  if cmd == 'trans'
    result = transactions_per_millisecond(items[0], items[1])
    puts "transactions_per_millisecond(#{items[0]}, #{items[1]}) = #{result}"
  elsif cmd == 'diff'
    result = largest_difference(items)
    puts "largest_difference(#{items.inspect}) = #{result.inspect}"
  elsif cmd == 'herei' || cmd == 'heres'
    start = items.shift
    stop  = items.shift
    result = here_to_there(items, start, stop)
    puts "here_to_there(#{items.inspect}, #{start.inspect}, #{stop.inspect}) = #{result.inspect}"
  else
    puts "Unrecognized command #{cmd}"
  end
end

def main
  print "Enter command: trans, diff, herei, heres: "
  cmd = (gets || "").chomp
  print "Enter data: "
  while (items = gets)
    items = items.split
    if cmd != 'heres'
      items.map! { |x| x.to_i }
    end
    apply_operation_to_array(cmd, items)
    print "Enter data: "
  end
end

if $0 == __FILE__
  main
end
