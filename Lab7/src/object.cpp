// object.cpp: implementation for reference-counted objects
// Make no changes to this file.

#include "object.h"
#include <iostream>

int CountedObject::object_count = 0;

CountedObject::CountedObject() {
  ++object_count;
}

CountedObject::~CountedObject() {
  --object_count;
}
