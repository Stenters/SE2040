
def foreachJump(array)
  for x in 1..(array.length - 1)
    yield(array[x], array[x - 1])
  end
  # array.each_index { |x| yield(array[x], array[x - 1])}
end

def addAll(array)
  sum = 0
  array.each { |x| sum += x.to_i }
  return sum
end

def main
  array = [0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0]
  prints = proc { |x| p x}
  
  diffs = foreachJump(array) { |x,y| p "dx / dy: #{x - y}" }
end


main