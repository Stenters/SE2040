# SE 2040
# Spring 2019
# Assignment: Lab 1
# Author: Stuart Enters

class Lab1

  def main()
    puts "Enter times, one per line:"
    times = []
    
    begin
    
      while line = gets
        times << line.to_i
      end
    rescue Exception => e
      puts findStats(times)
    end
    
    puts findStats(times)
    
  end


  def findStats(times)
    avg = averageTimes(times)
    sum = sumTimes(times)
    improved = improvedTimes(times)
    slower = slowerTimes(times)
    
    return "Average time: #{avg}
Weighted sum after ignoring all but the last five times: #{sum}
Number of improved times: #{improved}
Number of slower times: #{slower}"
  end

  def averageTimes(array)
    return (array.reduce(:+) / array.length.to_f).round(2)
  end


  def sumTimes(array)
    newArry = array.reverse[0..4]
    sum = 0
    newArry.each_index { |i| sum += newArry[i] / (2.0 ** i) }
    return calculateWeightedSum(sum, newArry)
  end

  
  def calculateWeightedSum(sum, newArry)
    count = newArry.length
    if count < 5      
      sum /= ( (2.0 * (2.0 ** (count - 1.0)) - 1.0) / (2.0 ** (count - 1.0)) )
    else  
      sum /= (1.9375)
    end
    return sum.round(2)
  end
  

  def improvedTimes(array)
    i = 1
    count = 0
    while i < array.length
      if array[i] < array[i - 1]
        count += 1
      end 
      
      i += 1
    end
    
    return count
  end


  def slowerTimes(array)
    i = 1
    count = 0
    while i < array.length
      if array[i - 1] < array[i]
        count += 1
      end
      i += 1
    end
    
    return count
  end
end

Lab1.new.main()