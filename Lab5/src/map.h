//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 5
// Author:     Stuart Enters
//

#ifndef MAP_H_
#define MAP_H_


// will not need to include cell.h
class MapCell;
#include <iostream>
using namespace std;

// Track the area the robot in which the robot moves.
// Note you will need to add declarations for the height and width.
// Add other methods and data as needed.
class Map {
private:
	static constexpr int WIDTH = 20;
	static constexpr int HEIGHT = 10;
    MapCell *cells[HEIGHT][WIDTH];

    // parse a single line of input
    void parseLine(string line, int lineNum);
public:
  // initialize empty map
  Map();
  // read the map from cin
  void load();
  // write the full map to cout
  void write();
  // get a cell from the map
  MapCell* getCell(int x, int y);
};

#endif /* MAP_H_ */
