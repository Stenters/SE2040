/*
 * doc.cpp
 *
 *  Created on: Apr 30, 2019
 *      Author: enterss
 */

#include "doc.h"
#include "line.h"
#include <iostream>
#include <fstream>
using namespace std;

Document::Document(){
    text = *(new std::list<Line*>);
}

Document::~Document(){
    for (auto i : text) {
        i->~CountedObject();
        delete(i);
    }
    this->~CountedObject();
}

// read lines from specified file
void Document::read(std::string source_file){
  string line = "";

  ifstream file;
  file.open(source_file);

  if (!file.is_open()){
      cout << "Unable to open file " << source_file << endl;
  }

  getline(file, line);
  while (file.good()) {
      text.push_back(new Line(line));
      getline(file, line);

  }

  file.close();

}

// add single line to end of current document
void Document::append(std::string new_line){
  text.push_back(new Line(new_line));
}

// insert line just before specified line number
void Document::insert(int before, std::string new_line){
  text.insert(getIterator(before), new Line(new_line));
}

// delete specified line
void Document::remove(int num){
  auto i = (getIterator(num));
  text.erase(i);
  (*i)->~CountedObject();
  delete(*i);
}

// delete lines between start and end, inclusive
void Document::remove(int start, int end){
  for (std::list<Line*>::const_iterator i = getIterator(start); i != getIterator(end + 1); ++i){
      (*i)->~CountedObject();
      delete(*i);
  }
  text.erase(getIterator(start), getIterator(end + 1));
}

// list contents of document to cout
void Document::list(){
    int i = 0;
    for (std::list<Line*>::const_iterator it = text.begin(); it != text.end(); ++it){
        if ((*it)->to_string() != "") {
            string tens = i > 9 ? std::to_string((i / 10) % 10) : " ";
            cout << " " << tens << i % 10 << ": " << (*it)->to_string() << endl;
            ++i;
        }
    }
}

// return map of line numbers to lines containing target_text, starting from
//    specified line
std::map<int, Line*> Document::find(std::string target_text, int start){
    map<int, Line*> map;
    int i = start;

    for (std::list<Line*>::const_iterator it = getIterator(start); it != text.end(); ++it) {
      if ((*it)->contains(target_text)){
          map[i] = *it;
      }
      ++i;
    }

    return map;
}

std::map<int, Line*> Document::otherFind(std::string target_text, int start) {
    cout << "text is : " << endl;
    for (auto line : text) {
        cout << (*line).to_string() << endl;
    }
    cout << "\t length: " << text.size() << "; 1st line: " << (*getIterator(0))->to_string() << "; last line: " << (*getIterator(text.size() - 1))->to_string() << endl;

    std::map<int, Line*> *map = new std::map<int, Line*>();
    std::list<Line*>::const_iterator it = getIterator(start);

    cout << "i is 0:" << text.size() << endl;

    for (unsigned i = 0; i < text.size(); ++i) {
        cout << endl << "i = " << i << endl;
        cout << "it is " << (*it)->to_string() << endl;

        if ((*it)->contains(target_text)) {
            cout << "found '" << target_text << "' in the line: '" << (*it)->to_string() << "'" << endl;

            (*map)[i] = *it;
            cout << "just added '" << (*(*map)[i]).to_string() << "'" << endl;
        } else {
            cout << "no match" << endl;
        }

        cout << "it was: " << (*it)->to_string();
        advance(it, 1);
        cout << "\t\tnow is: " << (*it)->to_string() << endl;
    }

    for (auto line : *map) {
        cout << line.first << ": " << line.second->to_string() << endl;
    }

    return *map;
}

std::list<Line*>::const_iterator Document::getIterator(int offset){
    std::list<Line*>::const_iterator i = text.begin();
    advance(i, offset);
    return i;
}
