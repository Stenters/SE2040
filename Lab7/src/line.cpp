// line.cpp
// Author: enterss
// SE 2040, Spring 2019
//

#include "line.h"
#include "words.h"
#include <iostream>
#include <sstream>

using namespace std;

Line::Line(string source) {
  stringstream iss(source);
  string next;
  iss >> next;
  while ( iss ) {
    Word* word_from_pool = WordPool::instance()->get(next);
    Line::contents.push_back(word_from_pool);

    iss >> next;
  }
}

bool Line::contains(string target) const {
    bool retVal = false;

    for (auto it = contents.begin(); it != contents.end(); ++it){
        if (!retVal) {
            retVal = ((*it)->value() == target);
        }
    }

    return retVal;

}

string Line::to_string() const {
  string result = "";
  for(auto wp : contents) {
    if ( !result.empty() )
      result += ' ';
    result += wp->value();
  }
  return result;
}

