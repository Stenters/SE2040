//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 5
// Author:     Stuart Enters
//

#include "robot.h"
#include "cell.h"
#include "map.h"
#include <iostream>
using namespace std;

    Robot::Robot(Map *map, int startx, int starty){
        this->map = map;
        x = startx;
        y = starty;
        goldCount = 0;

        // Check if the cell you are spawning on has gold, then enter it
        goldCount += (map->getCell(x,y)->hasGold()) ? 1 : 0;
        map->getCell(x,y)->enter();

	}


    // display robot status in the form of "Robot at 6, 7 (1 gold)" to cout
    void Robot::displayStatus(){
        cout << "Robot at " << x << ", " << y << " (" << goldCount << " gold)";
    }


    // move robot in specified direction (e/w/s/n), returning true
    //   if was able to move in that direction
    bool Robot::move(char direction){
        // Init the directons
        int destX = x, destY = y;
        direction = tolower(direction);

        // Change direction based on what was input
        switch(direction) {

        case 'e':
            ++destX;
            break;

        case 's':
            ++destY;
            break;

        case 'w':
            --destX;
            break;

        case 'n':
            --destY;
            break;

        default:
            cout << "Illegal direction!";
            break;
        }

        // Check if new cell is within bounds
        if (0 > destX || destX > 19 || 0 > destY || destY > 9){
            return false;
        }

        // Find the destination and current cells
        if (map->getCell(x,y)->occupied()) {

        }

        MapCell *destCell = map->getCell(destX, destY);
        MapCell *currentCell = map->getCell(x, y);

        // Check if destination is empty
        if (destCell->occupied()){
            return false;

        } else {
            // Pickup gold, enter new cell, then leave old
            goldCount += destCell->hasGold() ? 1 : 0;
            destCell->enter();
            currentCell->vacate();

            x = destX;
            y = destY;
            return true;
        }
    }


    // move robot in a series of directions, returning true if was
    //   able to complete the list of directions
    bool Robot::move(std::string commands) {
        return (commands != "") ? (move(commands[0])) ? move(commands.substr(1, commands.length())) : false : true;
    }


