/*
 * Exercise2.cpp
 *
 *  Created on: Mar 21, 2019
 *      Author: enterss
 */

#include <iostream>
using namespace std;

int read20Vals(int (&arry)[20], const int size){
    cout << "Please enter 20 values to read: ";
    for (int i = 0; i < size; ++i) {
        cin >> arry[i];
    }
    cout << endl;
    return 0;
}


int sortArry(int (&arry)[20], const int size){
    // for (int i : arry) {
    for (int i = 0; i < size; ++i) {
        int key = arry[i];
        int j = i - 1;
        while (j > -1 && arry[j] > key){
            arry[j + 1] = arry[j];
            --j;
        }
        arry[j + 1] = key;
    }

    return 0;
}


int printArry(int (&arry)[20], const int size){
    cout << "Array was: " << endl;
    for (int i = 0; i < size; ++i){
        cout << "arry[" << i << "] = " << arry[i] << endl;
    }

    return 0;
}


int main1(){
    int arry[20] = { 0 };
    read20Vals(arry, 20);
    sortArry(arry, 20);
    int arry2[20] = {5,};
    printArry(arry, 20);


    return 0;
}
